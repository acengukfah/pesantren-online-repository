<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Angkatan extends Model
{
    protected $primaryKey = 'id_angkatan';
    protected $fillable = ['nama_angkatan'];

    public function ujians()
    {
        return $this->belongsToMany('App\Ujian', 'angkatans_memiliki_ujians', 'id_angkatan', 'id_ujian');
    }
}
