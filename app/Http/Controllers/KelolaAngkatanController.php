<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Angkatan;
use Session;

class KelolaAngkatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $angkatans = Angkatan::all();
        return view('kelola_angkatan.index', compact('angkatans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $angkatan = new Angkatan();
        $angkatan->nama_angkatan = $request->nama_angkatan;
        $angkatan->save();

        Session::flash('sukses', 'Data Angkatan Berhasil Disave!');

        return redirect('/kelola_angkatan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $angkatan = Angkatan::find($id);
        $angkatan->update([
            'nama_angkatan' => $request->nama_angkatan
        ]);
        Session::flash('sukses', 'Data Angkatan Berhasil Diedit!');

        return redirect('/kelola_angkatan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Angkatan::find($id)->delete();
        Session::flash('sukses', 'Data Angkatan Berhasil Dihapus!');

        return redirect('/kelola_angkatan');
    }
}
