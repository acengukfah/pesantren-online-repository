<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Angkatan;
use App\Jamaah;
use Session;
use Illuminate\Support\Facades\DB;

class KelolaUserController extends Controller
{

    public function index()
    {
        $users = User::all();
        $angkatans = Angkatan::all();
        return view('kelola_user.index', compact('users', 'angkatans'));
    }
    public function verified(Request $request, User $user)
    {

        $user->update([
            'role' => $request->role
        ]);
        if ($request->role == 'jamaah') {
            $jamaah = new Jamaah();
            $jamaah->total_poin = 0;
            $jamaah->total_durasi = 0;
            $jamaah->id_angkatan = $request->angkatan;
            $jamaah->id_user = $user->id;
            $jamaah->save();
        }

        Session::flash('sukses', 'Data User Berhasil Diverified!');

        return redirect('/kelola_user');
    }

    public function update(Request $request, $id)
    {
        $id = User::find($id);
        $id->update([
            'role' => $request->role,
            'email' => $request->email,
            'name' => $request->name
        ]);
        // if ($request->role == 'jamaah') {
        //     $jamaah = Jamaah::where('id_user', $id)->first();
        //     $jamaah->update([
        //         'id_angkatan' => $request->id_angkatan,
        //     ]);
        // }

        Session::flash('sukses', 'Data User Berhasil Disave!');

        return redirect('/kelola_user');
    }
    public function destroy($id)
    {
        User::find($id)->delete();
        Session::flash('sukses', 'Data User Berhasil Dihapus!');

        return redirect('/kelola_angkatan');
    }
}
