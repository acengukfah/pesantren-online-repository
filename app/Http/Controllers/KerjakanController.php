<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Ujian;
use App\Jamaah;
use App\JamaahUjian;
use App\JamaahSoal;

class KerjakanController extends Controller
{
    
    public function index(){
        //$hasils = Auth::user()->jamaah->
        return view('kerjakan.index');
    }
    public function hasil(JamaahUjian $jamaah_ujian){
        return view('kerjakan.hasil', compact('jamaah_ujian'));
    }
    public function ujian(Ujian $ujian)
    {
        $jamaah = new Jamaah();
        $jamaah = Auth::user()->jamaah;
        if (JamaahUjian::where('id_jamaah','=', $jamaah->id_jamaah)->where('id_ujian', '=', $ujian->id_ujian)->exists()) {
            $jamaah_ujian = JamaahUjian::where('id_jamaah','=', $jamaah->id_jamaah)->where('id_ujian', '=', $ujian->id_ujian)->first();
        }else{
            $jamaah_ujian = new JamaahUjian();
            $jamaah_ujian->hasil_poin = 0;
            $jamaah_ujian->id_jamaah = $jamaah->id_jamaah;
            $jamaah_ujian->id_ujian = $ujian->id_ujian;
            $jamaah_ujian->save();
            foreach ($ujian->soals as $soal) {
                $jamaah_soal = new JamaahSoal();
                $jamaah_soal->id_jamaah_mengerjakan_ujian = $jamaah_ujian->id_jamaah_mengerjakan_ujian;
                $jamaah_soal->id_soal = $soal->id_soal;
                $jamaah_soal->save();
            }
        }
        return view('kerjakan.ujian', compact('jamaah_ujian'));
    }

    public function store(Request $request, JamaahUjian $jamaah_ujian)
    {
        $hasil = 0;
        foreach ($jamaah_ujian->jamaah_soals as $jamaah_soal) {
            $id_soal = $jamaah_soal->id_soal;
            $jamaah_soal->update([
                'id_jawab' => $request->$id_soal
            ]);
            if ($jamaah_soal->id_jawab == $jamaah_soal->soal->id_kunci) {
                $hasil = $hasil + 1;
            }
        }
        $hasil_poin = ($hasil / count($jamaah_ujian->ujian->soals)) * $jamaah_ujian->ujian->max_poin;
        $jamaah_ujian->jamaah->total_poin = $jamaah_ujian->jamaah->total_poin + $hasil_poin;
        $jamaah_ujian->jamaah->update([
            'total_poin' => $jamaah_ujian->jamaah->total_poin
        ]);
        $jamaah_ujian->update([
            'hasil_poin' => $hasil_poin
        ]);
        return view('kerjakan.hasil', compact('jamaah_ujian'));
    }
}
