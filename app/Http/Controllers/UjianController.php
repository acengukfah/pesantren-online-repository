<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ujian;
use App\Kategori;
use App\Week;
use App\Soal;
use App\Pilihan;
use Session;

class UjianController extends Controller
{
    public function index()
    {
        $ujians = Ujian::all();
        $kategoris = Kategori::all();
        $weeks = Week::all();
        return view('kelola_ujian.index', compact('ujians', 'kategoris', 'weeks'));
    }

    public function create(Request $request, Kategori $kategori)
    {
        $nama_ujian = $request->nama_ujian;
        $max_poin = $request->max_poin;
        $id_week = $request->id_week;
        $jumlah_soal = $request->jumlah_soal;
        $jumlah_pilihan = $request->jumlah_pilihan;

        return view('kelola_ujian.create', compact('nama_ujian', 'max_poin', 'id_week', 'jumlah_soal', 'jumlah_pilihan', 'kategori'));
    }

    public function store(Request $request)
    {
        //menyimpan data
        $kategori = new Kategori();
        $kategori->nama_kategori = $request->nama_kategori;
        $kategori->save();

        Session::flash('sukses', 'Kategori Baru Berhasil Dibuat!');

        return redirect('/kelola_ujian');
    }

    public function save(Request $request)
    {
        $ujian = new Ujian();
        $ujian->nama_ujian = $request->nama_ujian;
        $ujian->max_poin = $request->max_poin;
        $ujian->id_kategori = $request->id_kategori;
        $ujian->id_week = $request->id_week;
        $ujian->save();
        $k = 0;
        for ($i = 0; $i < $request->jumlah_soal; $i++) {
            $soal = new Soal();
            $soal->id_ujian = $ujian->id_ujian;
            $soal->isi_soal = $request->soal[$i];
            $soal->save();
            for ($j = 0; $j < $request->jumlah_pilihan; $j++) {
                $pilihan = new Pilihan();
                $pilihan->isi_pilihan = $request->pilihan[$k];
                $pilihan->id_soal = $soal->id_soal;
                $pilihan->save();
                if ($k == $request->kunci[$i]) {
                    $soal->update([
                        'id_kunci' => $pilihan->id_pilihan
                    ]);
                }
                $k++;
            }
        }

        Session::flash('sukses', 'Data Ujian Berhasil Disave!');

        return redirect('/kelola_ujian');
    }
}
