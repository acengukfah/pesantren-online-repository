<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jamaah extends Model
{
    protected $primaryKey = 'id_jamaah';
    protected $fillable = ['total_poin', 'total_durasi', 'id_angkatan', 'id_user'];

    public function angkatan(){
        return $this -> belongsTo('App\Angkatan', 'id_angkatan', 'id_angkatan');
    }
    public function jamaah_ujians(){
        return $this -> hasMany('App\JamaahUjian', 'id_jamaah', 'id_jamaah');
    }

    public function user(){
        return $this -> belongsTo('App\User', 'id_user', 'id');
    }
}
