<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JamaahSoal extends Model
{
    protected $primaryKey = 'id_jamaah_mengerjakan_soal';
    protected $fillable = ['id_jawab'];
    protected $table = 'jamaah_mengerjakan_soal';

    public function soal(){
        return $this -> belongsTo('App\Soal', 'id_soal', 'id_soal');
    }
}
