<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JamaahUjian extends Model
{
    protected $primaryKey = 'id_jamaah_mengerjakan_ujian';
    protected $table = 'jamaah_mengerjakan_ujian';
    protected $fillable = ['hasil_poin'];

    public function ujian(){
        return $this -> belongsTo('App\Ujian', 'id_ujian', 'id_ujian');
    }
    public function jamaah(){
        return $this -> belongsTo('App\Jamaah', 'id_jamaah', 'id_jamaah');
    }

    public function jamaah_soals(){
        return $this -> hasMany('App\JamaahSoal', 'id_jamaah_mengerjakan_ujian', 'id_jamaah_mengerjakan_ujian');
    }
}
