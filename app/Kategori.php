<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $primaryKey = 'id_kategori';

    public function ujians(){
        return $this -> hasMany('App\Ujian', 'id_kategori', 'id_kategori');
    }
}
