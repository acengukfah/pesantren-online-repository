<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pilihan extends Model
{
    protected $primaryKey = 'id_pilihan';
    protected $fillable = ['isi_pilihan', 'id_soal'];

    public function soal(){
        return $this -> belongsTo('App\Soal', 'id_soal', 'id_soal');
    }
}
