<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $primaryKey = 'id_soal';
    protected $fillable = ['isi_soal', 'id_ujian','id_kunci'];

    public function ujian(){
        return $this -> belongsTo('App\Ujian', 'id_ujian', 'id_ujian');
    }
    public function pilihans(){
        return $this -> hasMany('App\Pilihan', 'id_soal', 'id_soal');
    }
}
