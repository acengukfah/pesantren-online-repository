<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ujian extends Model
{
    protected $primaryKey = 'id_ujian';
    protected $fillable = ['nama_ujian', 'max_poin', 'id_kategori'];

    public function kategori(){
        return $this -> belongsTo('App\Kategori', 'id_kategori', 'id_kategori');
    }
    public function soals(){
        return $this -> hasMany('App\Soal', 'id_ujian', 'id_ujian');
    }
}
