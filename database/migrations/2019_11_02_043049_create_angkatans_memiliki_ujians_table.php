<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAngkatansMemilikiUjiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('angkatans_memiliki_ujians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table-> unsignedBigInteger('id_angkatan');
            $table-> unsignedBigInteger('id_ujian');
            $table->foreign('id_angkatan')->references('id_angkatan')->on('angkatans');
            $table->foreign('id_ujian')->references('id_ujian')->on('ujians');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('angkatans_memiliki_ujians');
    }
}
