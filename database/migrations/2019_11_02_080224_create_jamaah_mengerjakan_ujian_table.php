<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJamaahMengerjakanUjianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jamaah_mengerjakan_ujian', function (Blueprint $table) {
            $table->bigIncrements('id_jamaah_mengerjakan_ujian');
            $table->bigInteger('hasil_poin')->nullable();
            $table->unsignedBigInteger('id_jamaah');
            $table->unsignedBigInteger('id_ujian');

            $table->foreign('id_jamaah')->references('id_jamaah')->on('jamaahs');
            $table->foreign('id_ujian')->references('id_ujian')->on('ujians');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jamaah_mengerjakan_ujian');
    }
}
