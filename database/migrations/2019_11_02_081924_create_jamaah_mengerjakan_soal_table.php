<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJamaahMengerjakanSoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jamaah_mengerjakan_soal', function (Blueprint $table) {
            $table->bigIncrements('id_jamaah_mengerjakan_soal');
            $table->unsignedBigInteger('id_jamaah_mengerjakan_ujian');
            $table->unsignedBigInteger('id_soal');
            $table->unsignedBigInteger('id_jawab')->nullable();

            $table->foreign('id_soal')->references('id_soal')->on('soals');
            $table->foreign('id_jamaah_mengerjakan_ujian')->references('id_jamaah_mengerjakan_ujian')->on('jamaah_mengerjakan_ujian');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jamaah_mengerjakan_soal');
    }
}
