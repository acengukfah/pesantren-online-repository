<!DOCTYPE html>
<html>

<head>
	<title>Selamat Datang</title>
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<style type="text/css">
		.col-md-8 {
			background-color: #3dcb7b;
		}

		body {
			font-family: Montserrat;
		}

		.col-md-4 {
			height: 100vh;
			display: flex;
			justify-content: center;
			align-items: center;
		}

		.btn-custom {
			background-color: #3dcb7b;
			color: white;
		}

		.btn-custom:hover {
			background-color: #7aeeac;
			color: white;
		}

		.daftar {
			position: absolute;
			right: 18px;
			top: 18px;
			font-weight: bold;
			z-index: 10;
		}

		.home {
			position: absolute;
			left:18px;
			top:18px;
			font-weight: bold;
			z-index: 10;
		}
	</style>
</head>

<body>
	<div class="row no-gutters">
		<div class="daftar">
			<a href="{{ route('register') }}" style="color:#3dcb7b;">Daftar</a>
		</div>

		<div class="col-md-8">
			<!-- masih ksong bagian kiri -->
		</div>
		<div class="col-md-4">
			<div class="home">
				<a href="{{ url('/') }}" style="color:#3dcb7b;">Home</a>
			</div>
			<form method="POST" action="{{ route('login') }}">
				@csrf
				<div class="form-group">
					<label for="exampleInputEmail1" style="font-weight: bold;">Email</label>
					<input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
						name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

					@error('email')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1" style="font-weight: bold;">Password</label>
					<input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
						name="password" required autocomplete="current-password">

					@error('password')
					<span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
					@enderror
				</div>
				<div class="form-group form-check">
					<input class="form-check-input" type="checkbox" name="remember" id="remember"
						{{ old('remember') ? 'checked' : '' }}>

					<label class="form-check-label" for="remember">
						{{ __('Ingat saya') }}
					</label>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-custom" style="font-weight:bold;width: 100%;">
						{{ __('Login') }}
					</button>
				</div>
				<div class="form-group">
					@if (Route::has('password.request'))
					<a href="{{ route('password.request') }}" style="color:#3dcb7b">
						{{ __('Lupa password?') }}
					</a>
					@endif
				</div>
			</form>
		</div>

	</div>
</body>

</html>
{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

<div class="card-body">
	<form method="POST" action="{{ route('login') }}">
		@csrf

		<div class="form-group row">
			<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

			<div class="col-md-6">
				<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
					value="{{ old('email') }}" required autocomplete="email" autofocus>

				@error('email')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
			</div>
		</div>

		<div class="form-group row">
			<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

			<div class="col-md-6">
				<input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
					name="password" required autocomplete="current-password">

				@error('password')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
				@enderror
			</div>
		</div>

		<div class="form-group row">
			<div class="col-md-6 offset-md-4">
				<div class="form-check">
					<input class="form-check-input" type="checkbox" name="remember" id="remember"
						{{ old('remember') ? 'checked' : '' }}>

					<label class="form-check-label" for="remember">
						{{ __('Remember Me') }}
					</label>
				</div>
			</div>
		</div>

		<div class="form-group row mb-0">
			<div class="col-md-8 offset-md-4">
				<button type="submit" class="btn btn-primary">
					{{ __('Login') }}
				</button>

				@if (Route::has('password.request'))
				<a class="btn btn-link" href="{{ route('password.request') }}">
					{{ __('Forgot Your Password?') }}
				</a>
				@endif
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>
</div>
@endsection --}}