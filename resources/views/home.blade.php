@extends('layouts/app')

@section('title', 'Pesantren Online')

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 color-gray font-bold">Selamat Datang, {{Auth::user()->name}}!</h1>
  <a href="#"
    class="d-sm-inline-block btn btn-sm  btn-success bg-custom font-bold font-white shadow-sm">Panduan</a>
</div>
<!-- Content Row -->

<!-- Content Column -->

<!-- Project Card Example -->
@if(Auth::user()->role=='jamaah')
<div class="row">
  <div class="col-lg-12 ">
    <div class="card shadow  ">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold color-gray">Tugas saya -
          {{Auth::user()->jamaah->angkatan->nama_angkatan}}</h6>
      </div>
      <div class="card-body">
        <ul class="list-group list-group-flush">
          @if(Auth::user()->jamaah->jamaah_ujians->count() == Auth::user()->jamaah->angkatan->ujians->count())
          <p class="font-medium">Semua ujian anda sudah selesai dikerjakan</p>
          @endif
          @foreach(Auth::user()->jamaah->angkatan->ujians->sortByDesc('id_ujian') as $ujian)
          @if(DB::table('jamaah_mengerjakan_ujian')->where('id_jamaah','=',
          Auth::user()->jamaah->id_jamaah)->where('id_ujian', '=', $ujian->id_ujian)->exists())
          {{-- Tidak ada --}}
          @else
          <li class="list-group-item">
            <h5 class="card-title font-bold color-gray">{{$ujian -> nama_ujian}}</h5>
            <p class="card-text">{{$ujian->kategori->nama_kategori}}</p>
            <a type="btn bg-custom" href="/kerjakan/{{$ujian->id_ujian}}"
              class="btn btn-success bg-custom font-white font-bold" style="float:right;">Kerjakan</a>
          </li>
          @endif
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
@elseif(Auth::user()->role=='admin')
{{-- Tampilan admin --}}

<div class="row">
  <div class="col-md-6 ">
    <div class="card shadow  ">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold color-gray">Peringkat Poin Jamaah</h6>
      </div>
      <div class="card-body">
        <ul class="list-group list-group-flush">
          @foreach($jamaahs->sortByDesc('total_poin') as $jamaah)
          <li class="list-group-item">
            <p class="card-text font-medium color-gray">{{$loop->iteration}}# {{$jamaah->user->name}} <span class="font-bold font-green" style="float:right">{{$jamaah->total_poin}} poin</span></p>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>


{{-- akhir tampilan admin --}}
@else
<div class="row">
  <div class="col-md-7">
    <div class="card shadow">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold font-green">Tunggu sebentar</h6>
      </div>
      <div class="card-body">
        <p>Orang sabar disayang Allah SWT. Harap menunggu konfirmasi dari admin :)</p>
      </div>
    </div>
  </div>
</div>
@endif
@endsection