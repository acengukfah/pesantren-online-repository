@extends('layouts/app')

@section('title', 'Kelola Angkatan')

@section('content')

@if ($message = Session::get('sukses'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
    </div>
@endif

<!-- Content Row -->
<div class="row">

  <!-- Content Column -->
  <div class="col-lg-12 ">

    <!-- Project Card Example -->
    <div class="card shadow mb-4">
      <div class="card-header d-sm-flex align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold color-gray">Data Angkatan</h6>

        {{-- Tombol tambah kategori dengan modal --}}
        <button class="btn btn-link font-green font-bold d-none d-sm-inline-block"
                data-toggle="modal" data-target="#modalTambahKategori">
                +Tambah Angkatan
      </button>

        {{-- Modal tambah kategori --}}
        <div class="modal fade" id="modalTambahKategori" tabindex="-1" role="dialog"
            aria-labelledby="modalTambahKategoriTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title font-bold color-gray font-18"
                            id="modalTambahKategoriTitle">Tambah Angkatan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="/kelola_angkatan" method="POST">
                        @csrf
                        <div class="modal-body">
                            {{-- input data kategori --}}
                            <div class="form-group">
                                <label class="font-18 font-medium color-gray"
                                    for="namaKategori">Nama Angkatan</label>
                                <input type="text" class="form-control" id="namaKategori" name="nama_angkatan"
                                    placeholder="Nama Angkatan">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">Kembali</button>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Nama Angkatan</th>
                <th>Tanggal Buat</th>
                <th>Aksi</th>
              </tr>
            </thead>

            <tbody>
              @foreach($angkatans->sortByDesc('created_at') as $angkatan)
              <tr>
                <td>{{$angkatan->nama_angkatan}}</td>
                <td>{{$angkatan->created_at}}</td>
                <td>
                    <button  class="btn btn-success bg-custom font-white font-bold" data-toggle="modal" data-target="#modalEditAngkatan{{$angkatan->id_angkatan}}"><div class="fas fa-fw fa-edit"></button>
                    <a href="/kelola_angkatan/delete/{{$angkatan->id_angkatan}}" class="btn btn-danger bg-danger font-white font-bold"><logo class="fas fa-fw fa-trash"></logo></a>
                </td>                
              </tr>
              {{-- Modal tambah kategori --}}
              <div class="modal fade" id="modalEditAngkatan{{$angkatan->id_angkatan}}" tabindex="-1" role="dialog"
              aria-labelledby="modalEditAngkatan{{$angkatan->id_angkatan}}Title" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title font-bold color-gray font-18"
                              id="modalEditAngkatan{{$angkatan->id_angkatan}}Title">Tambah Angkatan</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <form action="/kelola_angkatan/update/{{$angkatan->id_angkatan}}" method="POST">
                          @csrf
                          <div class="modal-body">
                              {{-- input data kategori --}}
                              <div class="form-group">
                                  <label class="font-18 font-medium color-gray"
                                      for="namaKategori">Nama Angkatan</label>
                                  <input type="text" class="form-control" id="namaKategori" name="nama_angkatan"
                                value="{{$angkatan->nama_angkatan}}">
                              </div>
                          </div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-secondary"
                                  data-dismiss="modal">Kembali</button>
                              <button type="submit" class="btn btn-primary">Tambah</button>
                          </div>
                      </form>
                  </div>
              </div>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection