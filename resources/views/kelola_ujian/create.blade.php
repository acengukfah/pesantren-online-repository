@extends('layouts/app')

@section('title', 'Tambah Ujian')

@section('content')
<!-- Page Heading -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/kelola_ujian">Kelola ujian</a></li>
    <li class="breadcrumb-item" aria-current="page">{{$kategori->nama_kategori}}</li>
    <li class="breadcrumb-item active" aria-current="page">{{$nama_ujian}}</li>
  </ol>
</nav>
<form action="/kelola_ujian/{{$kategori->id_kategori}}" method="POST">
    @method('patch')
    @csrf
    <input type="text" name="jumlah_soal" value="{{$jumlah_soal}}" hidden>
    <input type="text" name="jumlah_pilihan" value="{{$jumlah_pilihan}}" hidden>
    <input type="text" name="nama_ujian" value="{{$nama_ujian}}" hidden>
    <input type="text" name="max_poin" value="{{$max_poin}}" hidden>
    <input type="text" name="id_kategori" value="{{$kategori->id_kategori}}" hidden>
    <input type="text" name="id_week" value="{{$id_week}}" hidden>
    {{-- Input soal --}}
    <div class="row">
        <div class="col">
            @php
                $k = 0;
            @endphp
            @for ($i = 0; $i < $jumlah_soal; $i++)  
            <div class="card shadow mb-3">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold color-gray">No. {{$i+1}}</h6>
                </div>
                <div class="card-body">
                    <textarea name="soal[{{$i}}]" class="form-control mb-3" rows="6" placeholder="Isi Soal..."></textarea>
                    {{-- pilihan --}}
                    @for ($j = 0; $j < $jumlah_pilihan; $j++)
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input name="kunci[{{$i}}]" value="{{$k}}" type="radio" >
                            </div>
                        </div>
                        <input type="text" name = "pilihan[{{$k}}]" class="form-control" placeholder="Isi pilihan">
                        @php
                            $k++;
                        @endphp
                    </div>
                    @endfor
                    {{-- akhir pilihan --}}
                </div>
            </div>
            @endfor
        </div>
    </div>
    <div class="row">
        <div class="col">
            <button type="submit" class="btn btn-success color-green font-bold">Simpan</button>
            <a href="/kelola_ujian" class="btn btn-link color-green font-bold">Kembali</a>
        </div>
    </div>
</form>
    
@endsection