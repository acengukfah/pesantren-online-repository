@extends('layouts/app')

@section('title', 'Kelola Ujian')

@section('content')

@if ($message = Session::get('sukses'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
    </div>
@endif

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 color-gray font-bold">Kategori Ujian</h1>
</div>
<!-- Content Row -->
<div class="row">
    <div class="col-lg-8">
        <div class="card shadow ">
            <div class="accordion  " id="accordionTambahKategori">
                <div class="card  rounded border-1">
                    <div class="card-header d-sm-flex justify-content-start" id="headingTambahKategori">
                        <h2 class="mb-0">
                            {{-- Tombol tambah kategori dengan modal --}}
                            <button class="btn btn-link font-bold font-green" type="button"
                                data-toggle="modal" data-target="#modalTambahKategori">
                                +Tambah Kategori
                            </button>

                            {{-- Modal tambah kategori --}}
                            <div class="modal fade" id="modalTambahKategori" tabindex="-1" role="dialog"
                                aria-labelledby="modalTambahKategoriTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title font-bold color-gray font-18"
                                                id="modalTambahKategoriTitle">Tambah Kategori</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="/kelola_ujian" method="POST">
                                            @csrf
                                            <div class="modal-body">
                                                {{-- input data kategori --}}
                                                <div class="form-group">
                                                    <label class="font-16 font-medium color-gray"
                                                        for="namaKategori">Nama kategori</label>
                                                    <input type="text" class="form-control" id="namaKategori" name="nama_kategori"
                                                        placeholder="Nama Kategori">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Kembali</button>
                                                <button type="submit" class="btn btn-primary">Tambah</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </h2>
                    </div>
                </div>
            </div>
            @foreach($kategoris as $kategori)
            <div class="accordion " id="accordion{{$kategori->id_kategori}}">
                <div class="card  rounded border-1">
                    <div class="card-header d-sm-flex align-items-center justify-content-between "  id="heading{{$kategori->id_kategori}}">
                        <h2 class="mb-0">
                            <button class="btn btn-link font-bold color-gray" type="button" data-toggle="collapse"
                                data-target="#collapse{{$kategori->id_kategori}}" aria-expanded="true"
                                aria-controls="collapse{{$kategori->id_kategori}}">
                                {{$kategori->nama_kategori}}
                            </button>
                        </h2>
                        <div>
                            <button class="btn btn-link font-bold font-green d-sm-inline-block" type="button"
                                data-toggle="modal" data-target="#modalTambahUjian{{$kategori->id_kategori}}">
                                +Tambah Ujian
                            </button>
                        </div>

                        {{-- Modal tambah Ujian --}}
                        <div class="modal fade" id="modalTambahUjian{{$kategori->id_kategori}}" tabindex="-1" role="dialog"
                            aria-labelledby="modalTambahUjian{{$kategori->id_kategori}}Title" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title font-bold color-gray font-18"
                                            id="modalTambahUjian{{$kategori->id_kategori}}Title">Tambah Ujian</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="/kelola_ujian/{{$kategori->id_kategori}}" method="POST">
                                        @csrf
                                        <div class="modal-body">
                                            {{-- input data Ujian --}}
                                            {{-- judulUjian --}}
                                            <div class="form-group">
                                                <label class="font-16 font-medium color-gray"
                                                    for="namaUjian">Nama Ujian</label>
                                                <input type="text" class="form-control" id="namaUjian" name="nama_ujian"
                                                    placeholder="Nama Ujian">
                                            </div>
                                            {{-- maxpoin (angka) --}}
                                            <div class="form-group">
                                                <label class="font-16 font-medium color-gray"
                                                    for="maxpoin">Poin Maksimal</label>
                                                <input type="number" class="form-control" id="maxpoin" name="max_poin"
                                                    placeholder="Poin Maksimal">
                                            </div>
                                            {{-- minggu ke (select box) --}}
                                            <div class="form-group">
                                                <label for="mingguKe" class="font-16 font-medium color-gray">Minggu ke-</label>
                                                <select class="form-control" id="mingguKe" name="id_week">
                                                    @foreach ($weeks as $week)
                                                    <option value="{{$week->id_week}}">{{$week->ke}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            {{-- jumlahsoal (angka) --}}
                                            <div class="form-group">
                                                <label class="font-16 font-medium color-gray"
                                                    for="jumlahSoal">Jumlah Soal</label>
                                                <input type="number" class="form-control" id="jumlahSoal" name="jumlah_soal"
                                                    placeholder="Jumlah Soal">
                                            </div>
                                            {{-- jumlah pilihan soal (angka) --}}
                                            <div class="form-group">
                                                <label class="font-16 font-medium color-gray"
                                                    for="jumlahPilihan">Jumlah Pilihan</label>
                                                <input type="number" class="form-control" id="jumlahPilihan" name="jumlah_pilihan"
                                                    placeholder="Jumlah Pilihan">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Kembali</button>
                                            <button type="submit" class="btn btn-primary">Tambah</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="collapse{{$kategori->id_kategori}}" class="collapse"
                        aria-labelledby="heading{{$kategori->id_kategori}}"
                        data-parent="#accordion{{$kategori->id_kategori}}">
                        
                        @foreach ($kategori->ujians as $ujian)
                        <ul class="list-group list-group-flush ">
                            <li class="list-group-item px-5">
                                <a class="color-gray" href="">{{$ujian->nama_ujian}}</a>
                            </li>
                        </ul>
                        @endforeach
                    </div>
                </div>
            </div>
            @endforeach
            
        </div>
    </div>
</div>
@endsection