@extends('layouts/app')

@section('title', 'Kelola User')

@section('content')

@if ($message = Session::get('sukses'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
    </div>
@endif

<!-- Content Row -->
<div class="row">

  <!-- Content Column -->
  <div class="col-lg-12 ">

    <!-- Project Card Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold color-gray">Data User</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Peran</th>
                <th>Angkatan</th>
                <th>Email</th>
                <th>Tanggal Daftar</th>
                <th>Aksi</th>
              </tr>
            </thead>

            <tbody>
              @foreach($users->sortByDesc('created_at') as $user)
              <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->role}}</td>
                @if($user->role == "jamaah")
                <td>{{$user->jamaah->angkatan->nama_angkatan}}</td>
                @else
                <td>-</td>
                @endif
                <td>{{$user->email}}</td>
                <td>{{$user->created_at}}</td>
                <td>
                  @if($user->role != "kosong")
                  <button href="" class="btn btn-success bg-custom font-white font-bold" data-toggle="modal" data-target="#modalEditUsers{{$user->id}}"><div class="fas fa-fw fa-edit"></div></button>
                  <a href="/kelola_user/delete/{{$user->id}}" class="btn btn-danger bg-danger font-white font-bold"><logo class="fas fa-fw fa-trash"></logo></a> 
                  
                  {{-- modal --}}
                  <div class="modal fade" id="modalEditUsers{{$user->id}}" tabindex="-1" role="dialog"
                    aria-labelledby="modalEditUsers{{$user->id}}Title" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title font-bold color-gray font-18"
                                    id="modalEditUsers{{$user->id}}Title">Edit User</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="/kelola_user/update/{{$user->id}}" method="POST">
                                @csrf
                                <div class="modal-body">
                                    {{-- input data kategori --}}
                                  <div class="form-group">
                                    <label class="font-18 font-medium color-gray"
                                        for="namaLengkap">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="namaLengkap" name="name"
                                    value="{{$user->name}}">
                                  </div>
                                  <div class="form-group">
                                    <label class="font-18 font-medium color-gray" for="role">Peran</label>
                                    <select class="form-control" id="role" name="role">
                                      @if ($user->role == 'jamaah')
                                      <option selected value="jamaah">Jamaah</option>
                                      <option value="ustadz">Ustadz</option>
                                      @else
                                      <option value="jamaah">Jamaah</option>
                                      <option selected value="ustadz">Ustadz</option>
                                      @endif
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label class="font-18 font-medium color-gray" for="angkatan">Nama Angkatan</label>
                                    <select class="form-control" id="angkatan" name="id_angkatan">
                                      @foreach ($angkatans as $angktn)
                                      @if ($user->jamaah['angkatan']['nama_angkatan'] == $angktn->nama_angkatan)
                                      <option selected value="{{$angktn->id_angkatan}}">{{$angktn->nama_angkatan}}</option>
                                      @else
                                      <option value="{{$angktn->id_angkatan}}">{{$angktn->nama_angkatan}}</option>
                                      @endif
                                      @endforeach
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label class="font-18 font-medium color-gray"
                                        for="email">Emaail</label>
                                    <input type="text" class="form-control" id="email" name="email"
                                    value="{{$user->email}}">
                                  </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-dismiss="modal">Kembali</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>

                  @elseif($user->email_verified_at == null)
                  <p>Belum verifikasi email</p>
                  @else
                  <button type="button" class="btn btn-primary font-bold" data-toggle="modal"
                    data-target="#modal{{$user->id}}">
                    Verifikasi
                  </button>

                  <!-- Modal -->
                  <div class="modal fade" id="modal{{$user->id}}" tabindex="-1" role="dialog"
                    aria-labelledby="modal{{$user->id}}Title" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="modal{{$user->id}}Title">Verifikasi User</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form action="/kelola_user/{{$user->id}}/verified">
                          <div class="modal-body">
                            <div class="form-group">
                              <label for="nama">Nama</label>
                              <input type="text" class="form-control" id="nama" value="{{$user->name}}" disabled>
                            </div>
                            <div class="form-group">
                              <label for="role">Peran</label>
                              <select class="form-control" id="role" name="role">
                                <option value="jamaah">Jamaah</option>
                                <option value="ustadz">Ustadz</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="role">Angkatan</label>
                              <select class="form-control" id="angkatan" name="angkatan">
                                @foreach ($angkatans as $angkatan)
                                <option value="{{$angkatan->id_angkatan}}">{{$angkatan->nama_angkatan}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                            <button type="submit" class="btn btn-success bg-custom font-white">Simpan</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection