@extends('layouts/app')

@section('title', 'Hasil - Tinjau')

@section('content')

<!-- Content Row -->
<div class="row">

    <!-- Content Column -->
    <div class="col-lg-12 ">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/hasil">Hasil</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$jamaah_ujian->ujian->nama_ujian}}</li>
            </ol>
        </nav>
        <!-- Project Card Example -->
        <div class="card shadow ">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold color-gray">Hasil Ujian</h6>
            </div>
            <div class="card-body">
                <h4 class="card-title large font-bold color-gray">
                    {{$jamaah_ujian->ujian->nama_ujian}}</h4>
                <p class="card-text">{{$jamaah_ujian->ujian->kategori->nama_kategori}}</p>
                <h6 class="mt-5 font-weight-bold color-gray">Skor anda<span
                        class="float-right">{{$jamaah_ujian->hasil_poin}}/{{$jamaah_ujian->ujian->max_poin}}</span>
                </h6>
                <div class="progress mb-4">
                    <div class="progress-bar bg-custom" role="progressbar" style="width: {{(($jamaah_ujian->hasil_poin)/$jamaah_ujian->ujian->max_poin)*100}}%"
                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                    </div>
                </div>
            </div>
        </div>
        <div class="card shadow mt-3">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold color-gray">Tinjau Soal</h6>
            </div>
            <div class="card-body font-medium">
                <ul class="list-group list-group-flush">
                    @foreach($jamaah_ujian->jamaah_soals as $jamaah_soal)
                    <li class="list-group-item color-gray">
                        <p class="card-text">{{$jamaah_soal->soal->isi_soal}} </p>
                        @foreach($jamaah_soal->soal->pilihans as $pilihan)
                        <div class="form-check">
                            @if($pilihan->id_pilihan == $jamaah_soal->id_jawab)
                            <input class="form-check-input " id="{{$pilihan->id_pilihan}}" type="radio"
                                name="{{$jamaah_soal->soal->id_soal}}" value="{{$pilihan->id_pilihan}}" checked
                                disabled>
                            @else
                            <input class="form-check-input" id="{{$pilihan->id_pilihan}}" type="radio"
                                name="{{$jamaah_soal->soal->id_soal}}" value="{{$pilihan->id_pilihan}}" disabled>

                            @endif
                            @if($pilihan->id_pilihan == $pilihan->soal->id_kunci)
                            <label class="form-check-label font-bold" style="color:#3dcb7b"
                                for="{{$pilihan->id_pilihan}}">
                                {{$pilihan->isi_pilihan}}
                            </label>
                            @else
                            <label class="form-check-label" for="{{$pilihan->id_pilihan}}">
                                {{$pilihan->isi_pilihan}}
                            </label>

                            @endif

                        </div>
                        @endforeach
                        @if($jamaah_soal->soal->id_kunci != $jamaah_soal->id_jawab)
                        <p class="font-bold" style="color:red">Salah!</p>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection