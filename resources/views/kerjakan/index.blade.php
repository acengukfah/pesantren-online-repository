@extends('layouts/app')

@section('title', 'Hasil')

@section('content')
<!-- Content Row -->
<div class="row">

    <!-- Content Column -->
    <div class="col-lg-12 ">

        <!-- Project Card Example -->
        <div class="card shadow ">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold color-gray">Total poin saya -
                    {{Auth::user()->jamaah->total_poin}} poin</h6>
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    {{-- INGATKAN --}}
                    {{-- Tambahkan kondisi jika tidak ada yang selesai --}}
                    @if(Auth::user()->jamaah->jamaah_ujians->count()==0)
                    <p class="font-medium">Anda belum pernah mengerjakan tugas</p>
                    @endif
                    @foreach(Auth::user()->jamaah->angkatan->ujians->sortByDesc('id_ujian') as
                    $ujian)
                    @if(DB::table('jamaah_mengerjakan_ujian')->where('id_jamaah','=',
                    Auth::user()->jamaah->id_jamaah)->where('id_ujian', '=',
                    $ujian->id_ujian)->exists())
                    <li class="list-group-item">
                        <h5 class="card-title font-bold color-gray">{{$ujian -> nama_ujian}}</h5>
                        <p class="card-text">{{$ujian->kategori->nama_kategori}}</p>
                        <div class="row">
                            {{-- bar --}}
                            <div class=" col-md-6">
                                <h6 class=" font-weight-bold color-gray">Skor anda
                                    <span class="float-right">
                                        {{DB::table('jamaah_mengerjakan_ujian')->where('id_jamaah','=', Auth::user()->jamaah->id_jamaah)->where('id_ujian', '=', $ujian->id_ujian)->first()->hasil_poin}}/
                                        {{DB::table('ujians')->where('id_ujian',DB::table('jamaah_mengerjakan_ujian')->where('id_jamaah','=', Auth::user()->jamaah->id_jamaah)->where('id_ujian', '=', $ujian->id_ujian)->first()->id_ujian)->first()->max_poin}}
                                    </span>
                                </h6>

                                <div class="progress mb-4">
                                    <div class="progress-bar bg-custom" role="progressbar" style="width: {{(DB::table('jamaah_mengerjakan_ujian')->where('id_jamaah','=',
                                                                    Auth::user()->jamaah->id_jamaah)->where('id_ujian', '=',
                                                                    $ujian->id_ujian)->first()->hasil_poin/$ujian->max_poin)*100}}%"
                                        aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <a type="btn bg-custom"
                                    href="/hasil/{{DB::table('jamaah_mengerjakan_ujian')->where('id_jamaah','=', Auth::user()->jamaah->id_jamaah)->where('id_ujian', '=', $ujian->id_ujian)->first()->id_jamaah_mengerjakan_ujian}}"
                                    class="btn btn-secondary font-bold" style="float:right;">Tinjau
                                    Kembali</a>
                            </div>

                        </div>
                    </li>
                    @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>

@endsection