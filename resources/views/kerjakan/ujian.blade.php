<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Pesantren Online - Ujian</title>

  <!-- Custom fonts for this template-->
  <link href="{{ URL::asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/sb-admin-2.min.css') }}" />
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700,900&display=swap" rel="stylesheet">


  <style>
    body {
      font-family: Montserrat;
    }

    .font-regular {
      font-weight: 400;
    }

    .font-medium {
      font-weight: 500;
    }

    .font-bold {
      font-weight: 700;
    }

    .font-extrabold {
      font-weight: 900;
    }

    .bg-custom {
      background: #3dcb7b;
    }

    .color-gray {
      color: #363636;
    }

    .font-white {
      color: white;
    }
  </style>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">



    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <form action="/hasil/{{$jamaah_ujian->id_jamaah_mengerjakan_ujian}}" method="post">
          @csrf

          <!-- Topbar -->
          <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">


            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">

              <!-- Nav Item - User Information -->
              <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false">
                  <span class="mr-2 d-none d-lg-inline text-gray-600">{{Auth::user()->name}}</span>
                  <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
                </a>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                  <a class="dropdown-item" href="#">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profile
                  </a>
                  <a class="dropdown-item" href="#">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Settings
                  </a>
                  <a class="dropdown-item" href="#">
                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                    Activity Log
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                  </a>
                </div>
              </li>

            </ul>

          </nav>
          <!-- End of Topbar -->

          <!-- Begin Page Content -->
          <div class="container-fluid">

            <!-- Content Row -->
            <div class="row justify-content-center">

              <!-- Content Column -->
              <div class="col-lg-8  ">

                <!-- Project Card Example -->
                <div class="card shadow mb-3">
                  <div class="card-body">
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item">
                        <h5 class="card-title font-bold color-gray large">{{$jamaah_ujian->ujian->nama_ujian}}</h5>
                        <p class="card-text">{{$jamaah_ujian->ujian->kategori->nama_kategori}}</p>
                        <button type="submit" class="mt-3 btn btn-success bg-custom font-white font-bold"
                          style="float:right; width: 100px; color:white;">Selesai</button>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card shadow font-medium">

                  <div class="card-body">
                    <ul class="list-group list-group-flush">
                      @foreach($jamaah_ujian->ujian->soals as $soal)
                      <li class="list-group-item color-gray">
                        <p class="card-text">{{$soal->isi_soal}} </p>
                        @foreach($soal->pilihans as $pilihan)
                        <div class="form-check">
                          <input class="form-check-input" id="{{$pilihan->id_pilihan}}" type="radio"
                            name="{{$soal->id_soal}}" value="{{$pilihan->id_pilihan}}">
                          <label class="form-check-label" for="{{$pilihan->id_pilihan}}">
                            {{$pilihan->isi_pilihan}}
                          </label>
                        </div>
                        @endforeach
                      </li>
                      @endforeach
                    </ul>
                    <div style="display:flex; justify-content:center;">
                      <button type="submit" class="mt-3 btn btn-success bg-custom font-white font-bold"
                        style="width: 100px; color:white;">Selesai</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- /.container-fluid -->

        </form>
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer ">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Pesantren Online 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Yakin untuk keluar?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Semua jawaban anda belum tersimpan jika anda keluar</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ URL::asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ URL::asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ URL::asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ URL::asset('js/sb-admin-2.min.js') }}"></script>

  <!-- Page level plugins -->
  <script src="{{ URL::asset('vendor/chart.js/Chart.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ URL::asset('js/demo/chart-area-demo.js') }}"></script>
  <script src="{{ URL::asset('js/demo/chart-pie-demo.js') }}"></script>

</body>

</html>
{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ujian</title>
</head>
<body>
    <form action="/kerjakan/{{$jamaah_ujian->id_jamaah_mengerjakan_ujian}}" method="POST">
@csrf
@foreach($jamaah_ujian->ujian->soals as $soal)
{{$loop->iteration}}. {{$soal->isi_soal}} <br>
@foreach($soal->pilihans as $pilihan)
<input type="radio" name="{{$soal->id_soal}}" value="{{$pilihan->id_pilihan}}">{{$pilihan->isi_pilihan}}
@endforeach
<br>
@endforeach
<button type="submit">selesai</button>
</form>
</body>

</html> --}}