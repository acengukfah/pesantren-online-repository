<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);


Route::group(['middleware' => ['auth', 'checkRole:admin']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/kelola_user', 'KelolaUserController@index');
    Route::get('/kelola_user/delete/{id}', 'KelolaUserController@destroy');
    Route::post('/kelola_user/update/{id}', 'KelolaUserController@update');
    Route::get('/kelola_user/{user}/verified', 'KelolaUserController@verified');
    Route::get('/kelola_angkatan', 'KelolaAngkatanController@index');
    Route::post('/kelola_angkatan', 'KelolaAngkatanController@store');
    Route::get('/kelola_angkatan/delete/{id}', 'KelolaAngkatanController@destroy');
    Route::post('/kelola_angkatan/update/{id}', 'KelolaAngkatanController@update');

    Route::get('/kelola_ujian', 'UjianController@index');
    Route::post('/kelola_ujian', 'UjianController@store');
    Route::post('/kelola_ujian/{kategori}', 'UjianController@create');
    Route::patch('/kelola_ujian/{kategori}', 'UjianController@save');
});

Route::group(['middleware' => ['auth', 'checkRole:admin,jamaah']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/kerjakan/{ujian}', 'KerjakanController@ujian');
    Route::post('/hasil/{jamaah_ujian}', 'KerjakanController@store');
    Route::get('/hasil/{jamaah_ujian}', 'KerjakanController@hasil');
    Route::get('/hasil', 'KerjakanController@index');
});

Route::group(['middleware' => ['auth', 'checkRole:admin,jamaah,kosong', 'verified']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
});
